import { Component, OnInit } from '@angular/core';
// import { Router, ActivatedRoute } from '@angular/router';
import { Product } from '../interfaces/product.interface';
import ALIMENTS from './../../assets/data/aliments.json';
import { filter } from 'minimatch';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {


  products: Product[];
  aliments = ALIMENTS;
  shoppingCart = [];
  totalPrice: number;
  productCounter: number;


  constructor(
    // private router: Router,
    // private route: ActivatedRoute,
  ) { }

  ngOnInit() {
  }

  addToCart(product) {

    this.shoppingCart.push(product);
    if (product.counter === 0) {
     
      product.counter++;
      
    }
    else if (product.counter > 0) {
      for (let i = 0; i < this.aliments.length; i++) {

        if (product.id === this.aliments[i].id) {
          product.counter++;
        }
      }
    }
    for(let i = 0; i < this.shoppingCart.length; i++){
      this.productCounter += this.shoppingCart[i].counter;
      this.totalPrice = this.productCounter * this.shoppingCart[i].price;
    }
console.log("El producto añadido es ", product);
    console.log("El carrito de la compra es ", this.shoppingCart);

  }





  deleteFromCart(product) {
    var i = this.shoppingCart.indexOf(product);
    this.shoppingCart.splice(i, 1);
    console.log("Se ha eliminado de la cesta ", product);
    console.log("La cesta de la compra esta compuesta por ", this.shoppingCart);
  }


  // filteredCoffee: Array<object> = []; 

  // filterCoffee(product){
  //  return product.category == 'coffee'
  // }

  // this.filteredCoffee = this.shoppingCart.filter(this.filterCoffee);

}



