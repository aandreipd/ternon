    export interface Product {
        id: string;
        category: string;
        name: string;
        price: number;
        counter:number
}

